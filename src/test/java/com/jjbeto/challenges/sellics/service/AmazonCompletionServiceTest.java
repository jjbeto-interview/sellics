package com.jjbeto.challenges.sellics.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AmazonCompletionServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmazonCompletionServiceTest.class);

    @Autowired
    private AmazonCompletionService amazonCompletionService;

    @Test
    public void testListIphone() {
        LOGGER.info("List: {}", amazonCompletionService.loadCompletionData("iphone", "aps"));
    }

}
