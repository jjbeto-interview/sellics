package com.jjbeto.challenges.sellics.service;

import com.jjbeto.challenges.sellics.dto.CalculationDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ScoreServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScoreServiceTest.class);

    @Autowired
    private ScoreService scoreService;

    @Test
    public void testIphone() {
        execute("iphone");
    }

    @Test
    public void testIphone8() {
        execute("iphone 8");
    }

    @Test
    public void testPocophone() {
        execute("pocophone");
    }

    @Test
    public void testIphoneCharger() {
        execute("iphone charger");
    }

    @Test
    public void testParalelepipedo() {
        execute("paralelepipedo");
    }

    @Test
    public void testLiquidificador() {
        execute("liquidificador");
    }

    @Test
    public void testGalaxyS7() {
        execute("galaxy s7");
    }

    @Test
    public void testAaa() {
        execute("aaa");
    }

    @Test
    public void testAaaBatteries() {
        execute("aaa batteries");
    }

    @Test
    public void testLinux() {
        execute("linux");
    }

    @Test
    public void testWindows() {
        execute("windows");
    }

    @Test
    public void testNintendo() {
        execute("nintendo");
    }

    @Test
    public void testGameOfThrones() {
        execute("game of thrones");
    }

    public void execute(final String query) {
        CalculationDto dto = scoreService.calculateScore(query);
        LOGGER.info("{} [{}]", query, dto.getScore());
    }

}
