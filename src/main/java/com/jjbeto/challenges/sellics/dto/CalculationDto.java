package com.jjbeto.challenges.sellics.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class CalculationDto {

    private String keyword;
    private double score;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String searchAlias;

    public CalculationDto(final String keyword) {
        if (keyword == null || keyword.trim().isEmpty()) {
            throw new IllegalArgumentException("Keyword cannot be null or empty.");
        }
        this.keyword = keyword.toLowerCase();
        score = 100.0;
    }

    public CalculationDto(final String keyword, final String searchAlias) {
        this(keyword);
        this.searchAlias = searchAlias;
    }

    public String getKeyword() {
        return keyword;
    }

    public Integer getScore() {
        return (int) score;
    }

    public String getSearchAlias() {
        return searchAlias;
    }

    public void increaseScore(double value) {
        if (score + value > 100) {
            score = 100;
        } else {
            score += value;
        }
    }

    public void decreaseScore(double value) {
        if (score - value < 0) {
            score = 0;
        } else {
            score -= value;
        }
    }

}
