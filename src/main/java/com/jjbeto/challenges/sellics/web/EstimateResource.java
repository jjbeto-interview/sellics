package com.jjbeto.challenges.sellics.web;

import com.jjbeto.challenges.sellics.dto.CalculationDto;
import com.jjbeto.challenges.sellics.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/estimate")
public class EstimateResource {

    @Autowired
    private ScoreService scoreService;

    @GetMapping
    public ResponseEntity<CalculationDto> getEstimate(@RequestParam String keyword, @RequestParam(required = false) String searchAlias) {
        if (keyword.trim().isEmpty()) {
            return ResponseEntity.badRequest().body(null);
        }

        final CalculationDto calculationDto = scoreService.calculateScore(keyword, searchAlias);
        return ResponseEntity.ok().body(calculationDto);
    }

}
