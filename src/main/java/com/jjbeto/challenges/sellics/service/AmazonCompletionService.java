package com.jjbeto.challenges.sellics.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AmazonCompletionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmazonCompletionService.class);

    @Value("${amazon.completion.endpoint}")
    private String amazonEndpoint;

    private RestTemplate restTemplate = new RestTemplate();
    private ObjectMapper mapper = new ObjectMapper();

    /**
     * Call Amazon's endpoint and return the result list of keyword hints. If no result is
     * found, returns an empty list.
     *
     * @param keyword
     * @param searchAlias
     * @return
     */
    public List<String> loadCompletionData(final String keyword, final String searchAlias) {
        try {
            final String json = restTemplate.getForObject(amazonEndpoint, String.class, searchAlias, keyword);
            final Object[] result = mapper.readValue(json, Object[].class);
            if (result != null && result.length > 1 && result[1] instanceof List) {
                return (List) result[1];
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return new ArrayList<>();
    }

}
