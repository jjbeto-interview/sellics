package com.jjbeto.challenges.sellics.service;

import com.jjbeto.challenges.sellics.dto.CalculationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

/**
 * A score is calculated based on the number of letters used to find the keyword in the result list.
 * For each letter needed to search, an equivalent number of points is removed from the fina result
 * (starting from 100). There is 2 break situations:
 * 1) Keyword is found (exact);
 * 2) Keyword is found as prefix (exact);
 * Extra points can be given in this situations (it's possible to configure it by parameters).
 */
@Service
public class ScoreService {

    private static final int MAX_POINTS = 100;

    @Autowired
    private AmazonCompletionService amazonCompletionService;

    @Value("${amazon.completion.endpoint.default.search-alias:aps}")
    private String defaultSearchAlias;

    @Value("${score.multiplier.exact:#{3}}")
    private Integer multiplierExact;

    @Value("${score.multiplier.exact.prefix:#{2}}")
    private Integer multiplierExactPrefix;

    public CalculationDto calculateScore(final String keyword, final String searchAlias) {
        final CalculationDto calculationDto;
        if (searchAlias != null && !searchAlias.trim().isEmpty()) {
            calculationDto = new CalculationDto(keyword, searchAlias);
        } else {
            calculationDto = new CalculationDto(keyword);
        }

        return processCalculation(calculationDto);
    }

    private CalculationDto processCalculation(final CalculationDto calculationDto) {
        final double pointsPerChar = loadPointsPerChar(calculationDto.getKeyword());
        final String searchAlias =
                calculationDto.getSearchAlias() == null ? defaultSearchAlias : calculationDto.getSearchAlias();
        final StringBuilder searchable = new StringBuilder();

        final Iterator<Character> iterator = calculationDto.getKeyword().chars().mapToObj(c -> (char) c).iterator();
        while (iterator.hasNext()) {
            searchable.append(iterator.next());
            final List<String> result = amazonCompletionService.loadCompletionData(searchable.toString(), searchAlias);

            if (matchExactKeyword(calculationDto, result)) {
                // if exact keyword is found, give extra points
                calculationDto.increaseScore(pointsPerChar * multiplierExact);
                return calculationDto;
            }

            if (matchExactKeywordAsPrefix(calculationDto, result)) {
                // if keyword is found as prefix for all hints, give extra points
                calculationDto.increaseScore(pointsPerChar * multiplierExactPrefix);
                return calculationDto;
            }

            calculationDto.decreaseScore(pointsPerChar);
        }

        return calculationDto;
    }

    /**
     * Return true if the EXACT keyword is found.
     *
     * @param calculationDto
     * @param result
     * @return
     */
    private boolean matchExactKeyword(final CalculationDto calculationDto, final List<String> result) {
        return result.stream().anyMatch(calculationDto.getKeyword()::equals);
    }

    /**
     * Return true if all results has the exact keyword as prefix.
     *
     * @param calculationDto
     * @param result
     * @return
     */
    private boolean matchExactKeywordAsPrefix(final CalculationDto calculationDto, final List<String> result) {
        return result.stream().allMatch(each -> each.startsWith(calculationDto.getKeyword()));
    }

    /**
     * Represents how much points this keyword will loose per call with no final result.
     * Explanation: for each letter used (starting from position 1) a percentage based on the keyword
     * length will be removed from overall points.
     *
     * @param keyword
     * @return points
     */
    private double loadPointsPerChar(final String keyword) {
        return MAX_POINTS / (keyword.length() - 1);
    }

}
