package com.jjbeto.challenges.sellics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SellicsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SellicsApplication.class, args);
	}

}
