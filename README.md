# Score Amazon's keywords

## What assumptions did you make?

* As soon as a keyword returns from the request, this means that this keyword is more important: 
Amazon should try to give a hint to the final user for the most used keywords during typing, 
so the best possibilities should appear first;
* As soon as we have all prefixes (from result) matching my complete keyword, we don't need to do 
more requests (the keyword will not return as an exact result anymore);
* An exact match is more important than no match;
* An exact match of all prefixes is more important than no match;
* Possibly an exact match is more important than matching all prefixes, but it's configurable by 
a multiplier weight;
* A keyword looses points for each extra char used to do a request to Amazon (means that the 
keyword is less important);
* Scenarios:
    1. Keyword found with one char (100 points): the keyword is so good that immediately Amazon 
    shows to the user as a hint;
    1. Keyword found after a couple os chars: a calculation is done per char to know how much 
    effort is necessary to get the keyword;
    1. Keyword found in all results as prefix: a calculation is done per char to know how much 
    effort is necessary to get the result (the exact match will not appear);
    1. Keyword is not found at all (0 points): if the keyword don't appear even as a partial 
    result, it's the worst case scenario;

## How does your algorithm work?

1. An arbitrary point is created based on the keyword's length: for each char after the first 
used to request data, the keyword will loose this number of points (until no more chars is left);
1. If keyword is found (exact): the keyword gain extra points (max 100) based on configured 
multiplier (property `score.multiplier.exact`): pointsPerChar * multiplier;
1. If keyword is found as prefix in all results (exact): the keyword gain extra points (max 100) 
based on configured multiplier (property `score.multiplier.exact.prefix`): pointsPerChar * multiplier;
1. If no results is found the calculation will be already `0`;

## Do you think the (*hint) that we gave you earlier is correct and if so - why?

Yes, it's correct. The order could represent the more important result, but in this case we need 
to know how much effort it is necessary to get the result, instead of the exact order it came in 
the top 10 results (in a really big range of possible results). The difference if it's in the 1st 
or 2nd place is irrelevant in a overall rating.

## How precise do you think your outcome is and why?

Due to the given information I think that it's quite precise, but I can't specify a quantifier 
number though. We don't have stored information (database) about previous executions, which could
give us extra data to get improve the score (for example we could try to get score for all categories
and for specific categories related to the keyword and return an average value).

## How to run

1. Execute class `com.jjbeto.challenges.sellics.SellicsApplication`;
1. Access link `http://localhost:8080/estimate?keyword=iphone+charger`;
1. Change the keyword as you need and retry;
1. The score will take into account by default `aps` (all departments), if you need to change the 
default value please change the configuration value property `amazon.completion.endpoint.default.search-alias`;
1. The keyword cannot be empty (like spaces), in this case a BAD_REQUEST (400) will be returned;

### Optional
1. Access link `http://localhost:8080/estimate?keyword=iphone&searchAlias=aps`;
1. Inform the different search alias to get score per category (source link [here](https://github.com/caroso1222/amazon-autocomplete/issues/6#issue-191788637)):
```html
<select class="nav-search-dropdown searchSelect" data-nav-digest="" data-nav-selected="0" id="searchDropdownBox" name="url" tabindex="18" title="Search in" style="top: 0px;">
<option selected="selected" value="search-alias=aps">All Departments</option>
<option value="search-alias=alexa-skills">Alexa Skills</option>
<option value="search-alias=instant-video">Amazon Video</option>
<option value="search-alias=warehouse-deals">Amazon Warehouse Deals</option>
<option value="search-alias=appliances">Appliances</option>
<option value="search-alias=mobile-apps">Apps &amp; Games</option>
<option value="search-alias=arts-crafts">Arts, Crafts &amp; Sewing</option>
<option value="search-alias=automotive">Automotive Parts &amp; Accessories</option>
<option value="search-alias=baby-products">Baby</option>
<option value="search-alias=beauty">Beauty &amp; Personal Care</option>
<option value="search-alias=stripbooks">Books</option>
<option value="search-alias=popular">CDs &amp; Vinyl</option>
<option value="search-alias=mobile">Cell Phones &amp; Accessories</option>
<option value="search-alias=fashion">Clothing, Shoes &amp; Jewelry</option>
<option value="search-alias=fashion-womens">&nbsp;&nbsp;&nbsp;Women</option>
<option value="search-alias=fashion-mens">&nbsp;&nbsp;&nbsp;Men</option>
<option value="search-alias=fashion-girls">&nbsp;&nbsp;&nbsp;Girls</option>
<option value="search-alias=fashion-boys">&nbsp;&nbsp;&nbsp;Boys</option>
<option value="search-alias=fashion-baby">&nbsp;&nbsp;&nbsp;Baby</option>
<option value="search-alias=collectibles">Collectibles &amp; Fine Art</option>
<option value="search-alias=computers">Computers</option>
<option value="search-alias=courses">Courses</option>
<option value="search-alias=financial">Credit and Payment Cards</option>
<option value="search-alias=digital-music">Digital Music</option>
<option value="search-alias=electronics">Electronics</option>
<option value="search-alias=gift-cards">Gift Cards</option>
<option value="search-alias=grocery">Grocery &amp; Gourmet Food</option>
<option value="search-alias=handmade">Handmade</option>
<option value="search-alias=hpc">Health, Household &amp; Baby Care</option>
<option value="search-alias=local-services">Home &amp; Business Services</option>
<option value="search-alias=garden">Home &amp; Kitchen</option>
<option value="search-alias=industrial">Industrial &amp; Scientific</option>
<option value="search-alias=digital-text">Kindle Store</option>
<option value="search-alias=fashion-luggage">Luggage &amp; Travel Gear</option>
<option value="search-alias=luxury-beauty">Luxury Beauty</option>
<option value="search-alias=magazines">Magazine Subscriptions</option>
<option value="search-alias=movies-tv">Movies &amp; TV</option>
<option value="search-alias=mi">Musical Instruments</option>
<option value="search-alias=office-products">Office Products</option>
<option value="search-alias=lawngarden">Patio, Lawn &amp; Garden</option>
<option value="search-alias=pets">Pet Supplies</option>
<option value="search-alias=pantry">Prime Pantry</option>
<option value="search-alias=software">Software</option>
<option value="search-alias=sporting">Sports &amp; Outdoors</option>
<option value="search-alias=tools">Tools &amp; Home Improvement</option>
<option value="search-alias=toys-and-games">Toys &amp; Games</option>
<option value="search-alias=vehicles">Vehicles</option>
<option value="search-alias=videogames">Video Games</option>
<option value="search-alias=wine">Wine</option>
</select>
```

## Notes

Some tests was developed (for services) just to help me during development and not to make asserts.
